package com.starsray.dynamic.ds.util;

import com.starsray.dynamic.ds.config.PrimaryDatasourceConfig;
import com.starsray.dynamic.ds.params.DatasourceProperties;

public class SQLUtils {


    public static String selectDs() {
        return "select * from dynamic_datasource";
    }

    public static String createTable() {
        return "create table if not exists dynamic_datasource (pool_name varchar(255) null, username varchar(255) null, password varchar(255) null, url varchar(255) null, driver_class_name varchar(255) null, create_time datetime default current_timestamp)";
    }

    public static String primaryDs(PrimaryDatasourceConfig primaryDatasourceConfig) {
        return "insert into dynamic_datasource (pool_name, username, password, url, driver_class_name) VALUES (" + "'" + primaryDatasourceConfig.getPrimary() + "'," + "'" + primaryDatasourceConfig.getUsername() + "'," + "'" + primaryDatasourceConfig.getPassword() + "'," + "'" + primaryDatasourceConfig.getUrl() + "'," + "'" + primaryDatasourceConfig.getDriverClassName() + "'" + ")";
    }

    public static String slaveDs(DatasourceProperties properties) {
        return "insert into dynamic_datasource (pool_name, username, password, url, driver_class_name) VALUES (" + "'" + properties.getPoolName() + "'," + "'" + properties.getUsername() + "'," + "'" + properties.getPassword() + "'," + "'" + properties.getUrl() + "'," + "'" + properties.getDriverClassName() + "'" + ")";
    }

    public static String removeDs(String poolName) {
        return "delete from dynamic_datasource where pool_name ='" + poolName + "' limit 1";
    }
}
