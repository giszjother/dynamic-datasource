package com.starsray.dynamic.ds.params;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * DsProperty
 *
 * @author starsray
 * @date 2021/11/16
 */
@Setter
@Getter
@ToString
public class DatasourceProperties {
    /**
     * 连接池名称
     */
    @NotBlank
    private String poolName;

    /**
     * JDBC driver org.h2.Driver
     */
    @NotBlank
    private String driverClassName;

    /**
     * JDBC url 地址
     */
    @NotBlank
    private String url;

    /**
     * JDBC 用户名
     */
    @NotBlank
    private String username;

    /**
     * JDBC 密码
     */
    @NotBlank
    private String password;
}