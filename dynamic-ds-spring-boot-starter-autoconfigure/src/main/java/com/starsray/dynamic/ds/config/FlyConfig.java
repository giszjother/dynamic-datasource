package com.starsray.dynamic.ds.config;

import com.starsray.dynamic.ds.flyway.FlywayLoad;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 动态配置
 *
 * @author starsray
 * @date 2023/03/23
 */
@Configuration
public class FlyConfig {

    @Bean(initMethod = "initFlyway")
    @ConditionalOnProperty(prefix = "spring.flyway", name = "enabled", havingValue = "true")
    public FlywayLoad flywayLoad() {
        return new FlywayLoad();
    }
}
