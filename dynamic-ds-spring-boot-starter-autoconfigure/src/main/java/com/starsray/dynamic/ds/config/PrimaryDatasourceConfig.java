package com.starsray.dynamic.ds.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "spring.datasource.dynamic.datasource.master")
@Configuration
@Data
public class PrimaryDatasourceConfig {

    /**
     * mybatis plus默认数据源 primary
     */
    private String primary = "master";
    private String driverClassName;
    private String url;
    private String username;
    private String password;
}
