package com.starsray.dynamic.datasource.controller;

import com.starsray.dynamic.datasource.annotation.DefaultDs;
import com.starsray.dynamic.datasource.bean.R;
import com.starsray.dynamic.datasource.constant.RedisKeys;
import com.starsray.dynamic.ds.core.DatasourceOperator;
import com.starsray.dynamic.ds.params.DatasourceProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Set;

/**
 * ds控制器
 *
 * @author starsray
 * @date 2023/02/23
 */
@RestController
@RequestMapping("ds")
public class DsOperatorController {

    @Resource
    private DatasourceOperator datasourceOperator;

    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("list")
    @DefaultDs
    public R<Set<String>> list() {
        return R.success(datasourceOperator.listDatasource());
    }

    @DefaultDs
    @PostMapping("addDatasource")
    public R<Set<String>> addDatasource(@RequestBody DatasourceProperties property) {
        Set<String> set = datasourceOperator.addDatasource(property);
        redisTemplate.convertAndSend(RedisKeys.ADD_QUEUE_CHANNEL, property);
        return R.success(set);
    }

    @DefaultDs
    @DeleteMapping("remove")
    public R<Boolean> remove(@RequestParam("poolName") String poolName) {
        boolean removeDatasource = datasourceOperator.removeDatasource(poolName);
        redisTemplate.convertAndSend(RedisKeys.DELETE_QUEUE_CHANNEL, poolName);
        return R.success(removeDatasource);
    }
}
