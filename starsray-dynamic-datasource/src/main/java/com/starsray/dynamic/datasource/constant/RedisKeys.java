package com.starsray.dynamic.datasource.constant;

public interface RedisKeys {
    String ADD_QUEUE_CHANNEL = "DS_ADD";
    String DELETE_QUEUE_CHANNEL = "DS_DELETE";
}
