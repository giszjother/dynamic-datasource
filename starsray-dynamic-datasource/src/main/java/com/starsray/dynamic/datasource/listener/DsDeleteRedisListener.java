package com.starsray.dynamic.datasource.listener;

import com.alibaba.fastjson.JSONObject;
import com.starsray.dynamic.ds.core.DatasourceOperator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 消息接收器侦听器
 *
 * @author starsray
 * @since 2021-11-17
 */
@Component
@Slf4j
public class DsDeleteRedisListener implements MessageListener {

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private DatasourceOperator datasourceOperator;


    @Override
    public void onMessage(Message message, byte[] pattern) {
        RedisSerializer<String> redisSerializer = redisTemplate.getStringSerializer();
        String s = redisSerializer.deserialize(message.getBody());
        String name = JSONObject.parseObject(s, String.class);
        Set<String> listDatasource = datasourceOperator.listDatasource();
        if (listDatasource.contains(name)) {
            datasourceOperator.removeDatasource(name);
            log.info("【多数据源】移除-重新加载，移除数据源：{}", name);
        }
    }

}