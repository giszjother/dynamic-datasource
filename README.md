#### 介绍
本项目基于dynamic-ds-spring-boot-starter进行演示多数据源的使用。
- DsInterceptor: AOP切换主要实现功能，具体可根据业务逻辑进行调整
- DynamicDataSourceContextHolder: mp提供的数据源操作工具类

#### 目录说明
- doc: mysql测试数据
- db/migration: flyway需要执行的脚本目录，更多使用方法参考flyway

#### 常见问题
- 数据源切换失败：排查是否在方法中进行了数据源切换失败，并且使用了@Transactional注解
- Druid版本过低：升级Druid版本至1.1.22以上
- flyway启动报错user_variables_by_thread无权限：对表进行授权
- 开启多数据源功能时，需要排除DruidDataSourceAutoConfigure类的自动注入，详情参考项目启动类
- 项目多节点集群问题：数据源信息在运行过程动态添加是存储在本地JVM内存的，可基于Redis监听广播到其他节点，详情参考代码实现RedisListenerConfig。

#### 其他
如果需要对多数据源进行扩展，可以实现com.baomidou.dynamic.datasource.processor.DsProcessor类，并注册该类的实现类，详情参考com.starsray.dynamic.datasource.config.MyDynamicDataSourceConfig。




